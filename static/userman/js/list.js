$(document).ready(function() {
    document.getElementById("id_district").value = "";
    document.getElementById("id_school").innerHTML = '<option>Выберите населенный пункт</option>';
    document.getElementById("id_school").disabled = true;

    //$.ajax({
    //    url: '/api/get_districts',
    //    type: 'GET',
    //    success: function(data) {
    //        var list1 = document.getElementById("list1");
    //        var text = "<option value='-1'></option>";
    //        for (var i = 0; i < data.length; i++) {
    //            text += "<option value='" + data[i].id + "'>" + data[i].name + "</option>";
    //        }
    //        list1.innerHTML = text;
    //        list1.disabled = false;
    //    },
    //    error: function(xhr, status, err) {
    //        console.log(status + "::  " + err);
    //    }
    //});

	document.getElementById("id_district").onchange = function() {
        var list1 = document.getElementById("id_district");
        var list2 = document.getElementById("id_school");

        if (list1.value == "") {
            list2.disabled = true;
            list2.innerHTML = '<option>Выберите населенный пункт</option>';
            return;
        }

        console.log("Making ajax query...");
        $.ajax({
            url: '/api/get_schools',
            type: 'GET',
            data: {'id': list1.value},
            success: function (data) {
                console.log("Data recieved");
                var text = "";
                for (var i = 0; i < data.length; i++) {
                    text += "<option value='" + data[i].id + "'>" + data[i].label + "</option>";
                }
                list2.innerHTML = text;
                list2.disabled = false;
            },
            error: function (xhr, status, err) {
                console.log(status + "::  " + err);
            },
            dataType: 'json'
        });
    };

	//console.log("Making ajax query");
	//$.ajax({
	//	url: '/api/get_schools',
	//	type: 'GET',
	//	data: {'id': '2'},
	//	success: function(data) {
	//		console.log("Data recieved");
	//		console.log(data);
	//	},
	//	dataType: 'json',
	//	error: function(xhr, status, err) {
	//		console.log(xhr);
	//		console.log(err);
	//		console.log(status);
	//	},
	//});
});
/**
 * Created by gars on 15.03.16.
 */
