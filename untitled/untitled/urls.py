"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^get_films/', 'webapi.views.get_films', name='films'),
    url(r'^get_seance/', 'webapi.views.get_seance', name='get_seance'),
    url(r'^get_cinemas/', 'webapi.views.get_cinemas', name='get_cinemas'),
    url(r'^get_genre/', 'webapi.views.get_genre', name='get_genre'),
    url(r'^delete_seance/', 'webapi.views.delete_seance', name='delete_seance'),
    url(r'^add_seance/', 'webapi.views.add_seance', name='add_seance')
]
