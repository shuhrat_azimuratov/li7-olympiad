from django.http import HttpResponse
from django.shortcuts import render
import json
# Create your views here.
from webapi.models import Film, Cinema, Genre


def get_genre(request):
    mimetype = 'application/json'
    data = []
    genres = Genre.objects.all()
    for genre in genres:
        tmp = {}
        tmp['name'] = genre.name
        tmp['id'] = genre.id
        data.append(tmp)
    res = json.dumps(data)
    return HttpResponse(res, mimetype)


def get_cinemas(request):
    mimetype = 'application/json'
    data = []
    cinemas = Cinema.objects.all()
    for cinema in cinemas:
        tmp = {}
        tmp['name'] = cinema.name
        tmp['id'] = cinema.id
        data.append(tmp)
    res = json.dumps(data)
    return HttpResponse(res, mimetype)


def get_seance(request):
    mimetype = 'application/json'
    data = []
    cinemas = Cinema.objects.all()
    for cinema in cinemas:
        tmp = {}
        tmp['cname'] = cinema.name
        tmp['cid'] = cinema.id
        tmp['films'] = []
        for film in cinema.seance.all():
            tmp2 = {}
            tmp2['name'] = film.name
            tmp2['age'] = film.age
            tmp2['genre'] = film.genre.name
            tmp2['gid'] = film.genre.id
            tmp2['film_id'] = film.id
            tmp['films'].append(tmp2)
        data.append(tmp)
    res = json.dumps(data)
    return HttpResponse(res, mimetype)


def get_films(request):
    mimetype = 'application/json'
    data = []
    films = Film.objects.all()
    for film in films:
        tmp = {}
        tmp['name'] = film.name
        tmp['age'] = film.age
        tmp['genre'] = film.genre.name
        data.append(tmp)
    result = json.dumps(data)
    return HttpResponse(result, mimetype)


def delete_seance(request):
    if request.GET.has_key("fid") and request.GET.has_key('cid'):
        fid = int(request.GET['fid'])
        cid = int(request.GET['cid'])
        cnm = Cinema.objects.get(id=cid).seance.remove(fid)
        return HttpResponse("Done")

    return HttpResponse("Wrong query")


def add_seance(request):
    if request.GET.has_key("gid") and request.GET.has_key("name") and request.GET.has_key('cid') \
            and request.GET.has_key('age'):
        gnr = Genre.objects.get(id=int(request.GET['gid']))
        Film.objects.create(name=request.GET['name'], age=int(request.GET['age']), genre=gnr)
        film = Film.objects.get(name__iexact=request.GET['name'])
        cinemas = str(request.GET['cid']).split(',')
        for cnm in cinemas:
            cinema = Cinema.objects.get(id=int(cnm))
            cinema.seance.add(film)
        print request.GET['gid'], request.GET['name'], request.GET['cid'], request.GET['age']
        return HttpResponse("Done")

    # film = Film.objects.get(name__iexact="hello")
    # if film is not None:
    #    print film.genre, film.name, film.age
    return HttpResponse("Wrong query")
