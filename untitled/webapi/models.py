from __future__ import unicode_literals

from django.db import models


# Create your models here.

class Genre(models.Model):
    name = models.CharField(max_length=150)

    def __unicode__(self):
        return  self.name

class Film(models.Model):
    name = models.CharField(max_length=150)
    age = models.IntegerField()
    genre = models.ForeignKey(Genre)

    def __unicode__(self):
        return  self.name


class Cinema(models.Model):
    name = models.CharField(max_length=150)
    seance = models.ManyToManyField(Film, null=True, blank=True)

    def __unicode__(self):
        return  self.name