from django.contrib import admin

# Register your models here.
from webapi.models import Film, Cinema
from webapi.models import Genre

admin.site.register(Film)
admin.site.register(Genre)
admin.site.register(Cinema)