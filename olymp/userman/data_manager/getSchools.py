# -*- coding: utf-8 -*-
# coding: utf8
import json

from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Firefox()
driver_worker = webdriver.Firefox()

driver.get("https://edu.tatar.ru/index.htm")

trs = driver.find_elements(By.TAG_NAME, "tr")

schools_list = []

for tr in trs:
    tds = tr.find_elements(By.TAG_NAME, "td")
    for td in tds:
        lis = td.find_elements(By.TAG_NAME, "li")
        for li in lis:
            if len(li.find_elements(By.TAG_NAME, "a")) == 0:
                continue
            url = str(li.find_elements(By.TAG_NAME, "a")[0].get_attribute("href")) + "/type/1"
            print url
            driver_worker.get(url)
            cols = driver_worker.find_elements_by_class_name("edu-list")
            schools = []
            fl = open("db/%s" % li.text, "w")
            # fl = open('/Users/shuhrat/li7-olympiad/olymp/userman/data_manager/db/%s' % li.text, 'w')
            for col in cols:
                names = col.find_elements(By.TAG_NAME, "li")

                for name in names:
                    fl.write(name.text)
                    schools.append(name.text)
                    # print(name.text)
            schools_list.append({"name": li.text, "schools": schools})
            fl.close()

f = open("schoolsList.json", "w")
f.write(json.dumps(schools_list, ensure_ascii=False))

driver.close()
driver_worker.close()
