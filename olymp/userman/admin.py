from django.contrib import admin
# Register your models here.
from check_app.models import ConfirmationPart
from userman.models import Contestant, Setting, Confirmation, School, Contact, Rayon


class ContestantAdmin(admin.ModelAdmin):
    list_display = ('get_contestant', 'is_active', 'gender', 'birth_date', 'sum')
    ordering = ('user__first_name', 'user__last_name', 'sum',)

    def is_active(self, obj):
        return obj.user.is_active

    def get_contestant(self, obj):
        return obj.user.first_name + ' ' + obj.fathers_name + ' ' + obj.user.last_name


class ConfirmationAdmin(admin.ModelAdmin):
    list_display = ('user', 'access_code', 'time_start')


class SchoolAdmin(admin.ModelAdmin):
    list_display = ('school', 'rayon')


class ContactFormAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'question_text', 'date', 'is_read', 'answered')
    ordering = ('is_read', 'answered', 'date')


class ConfirmAdmin(admin.ModelAdmin):
    list_display = ('contestant', 'will_come')


admin.site.register(Contestant, ContestantAdmin)
admin.site.register(Setting)
admin.site.register(Confirmation, ConfirmationAdmin)
admin.site.register(School, SchoolAdmin)
admin.site.register(Contact, ContactFormAdmin)
admin.site.register(Rayon)
admin.site.register(ConfirmationPart, ConfirmAdmin)
