# coding=utf-8
import datetime
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from nocaptcha_recaptcha import NoReCaptchaField

from check_app.models import CHOICES_CONFIRM
from olymp import settings
from userman.models import Contestant, School, Rayon


class ConfirmationForm(forms.Form):
    who = forms.ChoiceField(choices=CHOICES_CONFIRM, label=u'Введите номер телефона одного из родителей?')
    phone = forms.CharField(error_messages={'required': 'Введите номер телефона'},
                                   help_text=u'Введите номер телефона без 7. Например: 9171112233',
                                   label=u'Телефонный номер', widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Телефонный номер"
        }))
    will_come = forms.BooleanField(label='Вы будете участвовать?')


class LoginForm(forms.Form):
    username = forms.CharField(error_messages={'required': 'Введите ваш email'}, required=True,
                               widget=forms.TextInput(attrs={'placeholder': u'Пользователь или email',
                                                             'class': 'form-control'
                                                             }))
    password = forms.CharField(error_messages={'required': 'Введите ваш пароль'}, required=True,
                               widget=forms.PasswordInput(attrs={'placeholder': u'Пароль',
                                                                 'class': 'form-control'}))

    # nocaptcha = NoReCaptchaField(label=u'')

    def clean_password(self):
        username = self.cleaned_data["username"]
        password = self.cleaned_data["password"]

        if '@' in username:
            username = User.objects.filter(email=username).first()

        user = authenticate(username=username, password=password)
        if user is None:
            raise forms.ValidationError(u'Вы ввели неверный логин или пароль')
        return password


year = datetime.date.today().year
PRIVACY = u'privacy'
TERMS = u'terms'
CHOICES = [(PRIVACY, mark_safe(
    u'Cогласен(на) <a href=\"\" data-toggle="modal" data-target="#myModal">на обработку персональных данных</a>')),
           (TERMS, mark_safe(
               u'C <a href=\"\" data-toggle="modal" data-target="#myModal2">условиями</a> олимпиады ознакомлен(на) и принимаю их'))]


class RegisterForm(forms.Form):
    # username = forms.CharField()
    first_name = forms.CharField(error_messages={'required': 'Введите ваше имя'}, label=u'Имя',
                                 widget=forms.TextInput(attrs={
                                     'class': 'form-control',
                                     'placeholder': "Имя"
                                 }))
    last_name = forms.CharField(error_messages={'required': 'Введите вашу фамилию'}, label=u'Фамилия',
                                widget=forms.TextInput(attrs={
                                    'class': 'form-control',
                                    'placeholder': "Фамилия"
                                }))
    fathers_name = forms.CharField(error_messages={'required': 'Введите ваше отчество'}, label=u'Отчество',
                                   widget=forms.TextInput(attrs={
                                       'class': 'form-control',
                                       'placeholder': "Отчество"
                                   }))
    gender = forms.ChoiceField(error_messages={'required': 'Вы не выбрали пол'}, label=u'Пол',
                               choices=Contestant.GENDER_CHOICES, widget=forms.RadioSelect)
    email = forms.EmailField(error_messages={'required': 'Введите ваш email'}, label=u'email',
                             widget=forms.EmailInput(attrs={
                                 'class': 'form-control',
                                 'placeholder': "Email"
                             }))
    birth_date = forms.DateField(input_formats=('%d/%m/%Y',), error_messages={'required': 'Введите дату рождения'},
                                 label=u'Дата рождения',
                                 widget=forms.DateInput(format="%d/%m/%Y", attrs={
                                     'class': 'form-control',
                                     'placeholder': "Дата рождения: дд/мм/гггг"
                                 }))
    # todo add regex to the phone number
    phone_number = forms.CharField(error_messages={'required': 'Введите номер телефона'},
                                   help_text=u'Введите номер телефона без 7. Например: 9171112233',
                                   label=u'Телефонный номер', widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': "Телефонный номер"
        }))
    password = forms.CharField(error_messages={'required': 'Вы не ввели пароль'}, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': "Пароль"
    }), label=u'Пароль')
    repassword = forms.CharField(error_messages={'required': 'Повторите ввод пароля'},
                                 widget=forms.PasswordInput(attrs={
                                     'class': 'form-control',
                                     'placeholder': "Повторите пароль"
                                 }), label=u'Повторите пароль')
    district = forms.ModelChoiceField(empty_label=u'Выберите район', error_messages={'required': 'Вы не выбрали район'},
                                      queryset=Rayon.objects.all(),
                                      label=u'Населенный пункт',
                                      widget=forms.Select(attrs={
                                          'class': 'form-control'
                                      }))
    school = forms.ModelChoiceField(empty_label=u'Выберите школу', error_messages={'required': 'Вы не выбрали школу'},
                                    queryset=School.objects.all(),
                                    label=u'Школа', widget=forms.Select(attrs={
            'class': 'form-control',
            'disabled': ""
        }))

    terms = forms.MultipleChoiceField(error_messages={
        'required': u'Необходимо принять согласие на обработку персональных данных и условия участия на олимпиаде'},
        choices=CHOICES, label=u'', widget=forms.CheckboxSelectMultiple(attrs={
            'class': ''
        }))

    grade = forms.ChoiceField(error_messages={'required': 'Вы не выбрали класс'}, label=u'Класс',
                              choices=Contestant.GRADE_CHOICES, widget=forms.RadioSelect)
    teacher = forms.CharField(error_messages={'required': 'Введите ФИО вашего учителя'}, label=u'ФИО учителя',
                                 widget=forms.TextInput(attrs={
                                     'class': 'form-control',
                                     'placeholder': "ФИО учителя"
                                 }))

    # nocaptcha = NoReCaptchaField(label=u'')

    # school = forms.CharField(label=u'Школа', widget=forms.TextInput(attrs={
    # 'class': 'form-control '
    # }))

    def clean_terms(self):
        trm = self.cleaned_data["terms"]
        if len(trm) < 2:
            if trm[0] == PRIVACY:
                raise forms.ValidationError(u'Необходимо принять условия олимпиады')
            else:
                raise forms.ValidationError(u'Необходимо принять согласие на обработку персональных данных')
        elif len(trm) == 0:
            raise forms.ValidationError(
                u'Необходимо принять согласие на обработку персональных данных и условия участия на олимпиаде')
        return trm

    def clean_email(self):
        email = self.cleaned_data["email"]
        if len(User.objects.all().filter(email=email)) != 0:
            raise forms.ValidationError(u'Пользователь с таким email уже существует.')

        return email

    def clean_repassword(self):
        password = self.cleaned_data.get("password", "")
        repassword = self.cleaned_data.get("repassword", "")
        if password != repassword:
            raise forms.ValidationError('Пароли не сопадают')
        return repassword


class ContactForm(forms.Form):
    surname = forms.CharField(max_length=100, label=u'Фамилия', widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': "Фамилия *",
        'required': 'true'
    }))
    name = forms.CharField(max_length=150, label=u'Имя', widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': "Имя *",
        'required': 'true'
    }))
    email = forms.EmailField(label=u'Email', widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': "EMAIL *",
        'required': 'true'
    }))
    question_txt = forms.CharField(label=u'Текст вопроса', widget=forms.Textarea(attrs={
        'class': 'form-control',
        'placeholder': "Текст *",
        'required': 'true'
    }))
