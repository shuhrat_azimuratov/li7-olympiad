# coding=utf-8
from __future__ import unicode_literals
import datetime
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Rayon(models.Model):
    parent_rayon = models.ForeignKey('self', null=True, blank=True)
    rayon_name = models.CharField(max_length=150, verbose_name=u'Название района')

    def __unicode__(self):
        return self.rayon_name

    class Meta:
        verbose_name_plural = u'Районы'
        verbose_name = u'Район'


class School(models.Model):
    rayon = models.ForeignKey(Rayon, verbose_name=u'Район')
    school = models.CharField(max_length=400, verbose_name=u'Название школы')

    def __unicode__(self):
        return self.school

    class Meta:
        verbose_name = u'Школа'
        verbose_name_plural = u'Школы'


class Contestant(models.Model):
    MALE = 'M'
    FEMALE = 'F'
    user = models.OneToOneField(User, verbose_name=u'Участники олимпиады')
    fathers_name = models.CharField(max_length=30, verbose_name=u'Отчество')
    birth_date = models.DateField(verbose_name=u'Дата рождения')
    GENDER_CHOICES = ((MALE, u'Мужской'),
                      (FEMALE, u'Женский'))
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, verbose_name=u'Пол')
    phone_number = models.CharField(max_length=20, verbose_name=u'Телефонный номер')
    sum = models.IntegerField(blank=True, null=True)
    register_date = models.DateTimeField(auto_now_add=True, blank=True, verbose_name=u'Время регистрации')
    school = models.ForeignKey(School, verbose_name=u'Школа', blank=True, null=True)
    activation_key = models.CharField(blank=True, max_length=100, null=True)
    key_expires = models.DateTimeField(default=datetime.date.today())
    GRADE_CHOICES = ((5, u'5 класс'), (6, '6 класс'))
    grade = models.IntegerField(blank=True, null=True, choices=GRADE_CHOICES, verbose_name=u'Класс')
    finished = models.BooleanField(default=False)
    started = models.BooleanField(default=False)
    teacher = models.CharField(max_length=100, verbose_name=u'Учитель')

    class Meta:
        verbose_name = u'Участник'
        verbose_name_plural = u'Участники'

    def __unicode__(self):
        return str(self.user)


class Confirmation(models.Model):
    user = models.ForeignKey(Contestant)
    access_code = models.IntegerField()
    time_start = models.DateTimeField(auto_now_add=True, blank=True, verbose_name=u'Время отправки сообщения')


class Setting(models.Model):
    start_time = models.DateTimeField(auto_now_add=True, blank=True, verbose_name=u'Время начала олимпиады')
    confirm_live = models.TimeField(blank=True, verbose_name=u'Время жизни активационного кода')
    is_reg_opened = models.BooleanField(default=True, verbose_name=u'Регистрация открыта?')

    class Meta:
        verbose_name_plural = u'Настройки'
        verbose_name = u'Настройка'


class Contact(models.Model):
    surname = models.CharField(max_length=150, verbose_name=u'Фамилия')
    name = models.CharField(max_length=150, verbose_name=u'Имя')
    email = models.EmailField(verbose_name=u'Email')
    question_text = models.TextField(max_length=500, verbose_name=u'Вопрос')
    date = models.DateTimeField(auto_now_add=True, verbose_name=u'Время отправки запроса')
    is_read = models.BooleanField(default=False, verbose_name=u'Прочитано?')
    answered = models.BooleanField(default=False, verbose_name=u'Отвечено?')
