# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-05-16 15:43
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userman', '0004_auto_20160509_1214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contestant',
            name='key_expires',
            field=models.DateTimeField(default=datetime.date(2016, 5, 16)),
        ),
    ]
