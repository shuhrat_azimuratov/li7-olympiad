# coding=utf-8
import hashlib
import random
from string import lowercase

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
# Create your views here.
from django.utils import timezone
from django.views.generic import View
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.edit import FormMixin
from django.core.mail import EmailMessage, send_mail
import datetime
from userman.forms import LoginForm, RegisterForm, ContactForm
from userman.models import Contestant, School, Contact


def send_email():
    email = EmailMessage('Subject', 'Body', to=['shuhrat94@ya.ru'])
    email.send()


# todo Обработать активированных и неактивированных пользователей
# todo Add email notification about new question
def index(request):
    # if request.user.is_authenticated():
    #         #return HttpResponseRedirect(reverse('test-app:res'))
    #         return HttpResponseRedirect(reverse('test-app:home'))
    form = ContactForm()
    if request.POST:
        form = ContactForm(request.POST)
    if form.is_valid():
        Contact.objects.create(name=form.cleaned_data['name'],
                               surname=form.cleaned_data['surname'],
                               email=form.cleaned_data['email'],
                               question_text=form.cleaned_data['question_txt'], )
        return HttpResponseRedirect(reverse('contact'))
    context = {'form': form}
    # context['user'] = request.user.username
    # if request.user.is_active:
    #    print 'Active'
    # else:
    #    print 'Not active'

    # print context
    return render(request, 'userman/index.html', context)


class LoginView(View, TemplateResponseMixin, FormMixin):
    form_class = LoginForm
    template_name = "userman/login.html"
    success_url = reverse_lazy("main")
    tmp = {}
    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context["form"] = self.get_form(self.get_form_class())
        if self.request.GET.has_key("next"):
            context['next'] = self.request.GET['next']
        return context

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('main'))
        return self.render_to_response(self.get_context_data(), **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form(self.get_form_class())
        if form.is_valid():
            print "yeap"
            if self.request.GET.has_key("next") and self.request.GET['next'] != '':
                self.tmp['next'] = self.request.GET['next']
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):

        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        user = None
        if '@' in username:
            username = User.objects.filter(email=username).first()

        user = authenticate(username=username, password=password)
        if user is not None:
            login(self.request, user)

            if self.tmp.has_key("next"):
                return HttpResponseRedirect(self.tmp['next'])
            else:
                return HttpResponseRedirect(self.success_url)
        else:
            return self.form_invalid(self.request)

    def form_invalid(self, form):
        return self.get(self.request)


def log_out(request):
    logout(request)
    return HttpResponseRedirect(reverse('main'))


def activate(request):
    pass


def register(request):
    form = RegisterForm()
    if request.POST:
        form = RegisterForm(request.POST)
    if form.is_valid():
        string_val = "".join(random.choice(lowercase) for i in range(25))
        user = User.objects.create_user(username=string_val,
                                        email=form.cleaned_data['email'],
                                        password=form.cleaned_data['password'],
                                        first_name=form.cleaned_data['first_name'],
                                        last_name=form.cleaned_data['last_name'], is_active=False)
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        activation_key = hashlib.sha1(salt + form.cleaned_data['email']).hexdigest()
        key_expires = datetime.datetime.today() + datetime.timedelta(2)

        contestant = Contestant.objects.create(user=user,
                                               gender=form.cleaned_data['gender'],
                                               birth_date=form.cleaned_data['birth_date'],
                                               fathers_name=form.cleaned_data['fathers_name'],
                                               phone_number=form.cleaned_data['phone_number'],
                                               school=form.cleaned_data['school'],
                                               activation_key=activation_key,
                                               key_expires=key_expires,
                                               grade=form.cleaned_data['grade'],
                                               teacher=form.cleaned_data['teacher'])
        email_subject = 'Подтверждение пользователя mel-olymp.ru'
        email_body = u"Здравствуйте, %s. Спасибо за регистрацию. Чтобы активировать ваш пользователь, перейдите \
         по ссылке ниже в течение 48 часов http://mel-olymp.ru/confirm/%s" % (
            form.cleaned_data['email'], activation_key)
        send_mail(email_subject, email_body, 'Олимпиада МЕЛ<inbox@mel-olymp.ru>',
                  [form.cleaned_data['email']], fail_silently=False)
        user = authenticate(username=string_val, password=form.cleaned_data['password'])
        login(request, user)
        return HttpResponseRedirect(reverse('main'))
    return render(request, 'userman/register.html', {'form': form})


def register_confirm(request, activation_key):
    if request.user.is_authenticated() and request.user.is_active:
        return HttpResponseRedirect(reverse('test-app:home'))
    user_profile = get_object_or_404(Contestant, activation_key=activation_key)
    if user_profile.key_expires < timezone.now():
        return render(request, 'userman/register_expired.html')
    user = user_profile.user
    user.is_active = True
    user.save()
    return HttpResponseRedirect(reverse('test-app:home'))


def register_success(request):
    return render(request, 'userman/register_success.html')


def contact(request):
    return render(request, 'userman/contact_success.html')


def program(request):

    return render(request, 'userman/program.html')