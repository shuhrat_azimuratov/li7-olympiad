from django.conf.urls import url, patterns

urlpatterns = patterns('api.views',
                       # Examples:
                       # url(r'^$', 'tosterproject.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^get_schools/', 'get_schools', name='get_schools'),
                       url(r'^$', 'home', name='home'),
                       url(r'^get_districts/', 'get_districts', name='get_districts'),
                       url(r'^csv/', 'upload_data', name='upload-data')

                       )
