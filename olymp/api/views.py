# coding=utf-8
import json
import os

import requests
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
# Create your views here.
from django.shortcuts import render_to_response

from test_app.models import Subject, Question, Answer
from userman.models import School, Rayon
import csv


def get_schools(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        rayon_id = int(request.GET.get('id', ''))
        schools = School.objects.filter(rayon=rayon_id, school__icontains=q.lower())
        print schools
        results = []
        for school in schools:
            school_json = {}
            school_json['id'] = school.id
            school_json['label'] = school.school
            school_json['value'] = school.school
            results.append(school_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def home(request):
    return render(request, 'api/home.html')


def get_districts(request):
    data = 'fail'
    if request.is_ajax():
        districts = Rayon.objects.all()
        res = []
        for district in districts:
            distr_json = {}
            distr_json["id"] = district.id
            distr_json["name"] = district.rayon_name
            res.append(distr_json)
        data = json.dumps(res)
    mimetype = 'apllication/json'
    return HttpResponse(data, mimetype)


@login_required
def upload_data(request):
    path = os.path.join(os.curdir, 'userman', 'data_manager', 'math.csv')

    fl = open(path)
    subject = Subject.objects.get(name__icontains='Математика')
    print subject
    reader = csv.reader(fl, delimiter=';')

    sum = 0
    for row in reader:
        question = Question()
        s = ''
        number = int(row[0].split('.')[0])
        question.question_number = number
        text = "<p>"
        text += row[2]
        text += '</p>'
        question.text = text.replace('\n', '<br>')
        print question.text
        question.help_text = ' '
        question.subject = subject

        # print text, number
        answer = []
        rowt = row[1].replace("\xc2\xa0", "")
        for i in range(3, len(row)):
            if row[i] != '':
                answer.append({'text': row[i], 'correct': False})

        answer[0]['correct'] = True
        if rowt == 'RADIO':
            question.type = 'R'
        elif rowt == 'INPUTC':
            question.type = 'I'
        # question.save()
        for i in answer:
            answer = Answer()
            answer.question = question
            answer.text = i['text']
            print answer.text
            answer.is_correct = i['correct']
            # answer.save()
            # print answer

    return HttpResponse(os.path.abspath(path))
