function updateTimer(date) {
	var endTime = date;
	var nowTime = Date.now();
	var timeLeft = Math.max(0, parseInt((endTime - nowTime) / 1000));

	var s = timeLeft % 60;

	timeLeft /= 60;
	timeLeft = parseInt(timeLeft);
	var m = timeLeft % 60;
	timeLeft /= 60;
	timeLeft = parseInt(timeLeft);
	timeLeft = (timeLeft < 10 ? "0" : "") + timeLeft.toString();
	m = (m < 10 ? "0" : "") + m.toString();
	s = (s < 10 ? "0" : "") + s.toString();
	document.getElementById("timeLeft").innerHTML = timeLeft + ":" + m + ":" + s;
}
