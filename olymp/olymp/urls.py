"""olymp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import password_reset
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from olymp import settings
from userman.views import LoginView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', LoginView.as_view(), name='loginn'),
    url(r'^$', 'userman.views.index', name='main'),
    url(r'^register/', 'userman.views.register', name='register'),
    url(r'^logout/', 'userman.views.log_out', name='logoutt'),
    url(r'^accounts/registration/$',
        password_reset,
        {'html_email_template_name':
             'registration/password_reset_email_html.html', },
        name='registration',
        ),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^data/upload/', 'userman.data_manager.backup.upload', name='upload'),
    url(r"^api/", include('api.urls'), name="api"),
    url(r'^confirm/(?P<activation_key>\w+)/', 'userman.views.register_confirm', name='register_confirm'),
    url(r'^register_success/', 'userman.views.register_success', name='register_success'),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^contact/', 'userman.views.contact', name='contact'),
    url(r'^test/', include('test_app.urls', namespace='test-app')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}
        ),
    url(r'^check/', include('check_app.urls', namespace='check')),
    url(r'^program/', 'userman.views.program', name='program'),

]

urlpatterns += staticfiles_urlpatterns()