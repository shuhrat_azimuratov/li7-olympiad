# coding=utf-8
from __future__ import unicode_literals
from django.db import models
# Create your models here.
from userman.models import Contestant


class Result(models.Model):
    contestant = models.ForeignKey(Contestant, verbose_name=u'Участник', unique=True)
    score = models.IntegerField(verbose_name=u'Балл')

    class Meta:
        ordering = ['-score']


CHOICES_CONFIRM = (('M', u'Мама'), ('F', 'Папа'))


class ConfirmationPart(models.Model):
    contestant = models.OneToOneField(Contestant, verbose_name=u'Участник')
    will_come = models.BooleanField(verbose_name=u'Приедет?')
    who = models.CharField(choices=CHOICES_CONFIRM, max_length=1)
    phone_number = models.CharField(max_length=20, verbose_name=u'Нормер телефона')
