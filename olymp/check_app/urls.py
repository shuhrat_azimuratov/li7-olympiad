from django.conf.urls import patterns, include, url

urlpatterns = patterns('check_app.views',
                       # Examples:
                       # url(r'^$', 'tosterproject.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r"^$", 'index', name='home'),
                       url(r"^process/(?P<id>[0-9]+)/$", 'user_check', name='process'),
                       url(r"^results/$", 'results', name='results'),
                       url(r'^list/$', 'list', name='list'),
                       url(r'^pdf/(?P<uid>[0-9]+)/$', 'get_pdf', name='pdf'),
                       url(r'^stats/$', 'stats', name='stats'),
                       url(r'^will/$', 'get_list_of_participants', name='parts'),
                       url(r'^gen/(?P<uid>[0-9]+)/$', 'gen_pdf', name='gen'),
                       url(r'^gen2/(?P<uid>[0-9]+)/$', 'gen_pdf_onsite', name='gen2'),
                       url(r'^pdf2/(?P<uid>[0-9]+)/$', 'get_pdf_nosite', name='pdf2'),
                       url(r'^questions/(?P<subject_id>[0-9]+)$', 'questions_list',
                           name='questions'),
                       url(r'^questions/', 'questions_list', name='questions_list'),
                       url(r'^partstats/', 'statistics_participants', name='partstats')
                       # url(r'^login/$', LoginView.as_view(), name='login'),
                       # url(r'^logout/$', 'log_out', name='logout'),
                       # url(r'^class/(?P<class_id>\d+)/$', 'class_journal', name='class_journal'),
                       # url(r'^test/$', 'raiting2', name='raiting2'),
                       # url(r'^class/(?P<class_id>\d+)/master$', 'class_journal', name='my_class'),
                       # url(r'^student/(?P<student_id>\d+)/$', 'student_profile', name='student'),
                       # url(r'^raiting/$', 'raiting', name='raiting'),
                       # url(r'^inc/$', 'increase', name='inc')
                       # url(r'^event/(?P<event_id>\d+)/edit/$', 'event_edit', name='event_edit')
                       )
