class FormKeeper:
    form = None
    question_number = None
    question_text = None

    def __init__(self, form, question_number, question_text):
        self.form = form
        self.question_number = question_number
        self.question_text = question_text

    def __lt__(self, other):
        return self.question_number < other.question_number

    def __gt__(self, other):
        return self.question_number > other.question_number

