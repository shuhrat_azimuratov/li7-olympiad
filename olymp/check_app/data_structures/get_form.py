from test_app.forms import TextAreaForm, InputForm, CheckboxForm, RadioForm
from test_app.models import Question


def get_form(question):
    forms = {'R': RadioForm, 'C': CheckboxForm, 'I': InputForm, 'A': TextAreaForm}
    form = forms[question.type](question)
    return form
