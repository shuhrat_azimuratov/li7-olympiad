# coding=utf-8
from django import forms

from test_app.models import UserAnswer

CHOICES = ((None, u"Не знаю"),
           (True, u"Верно"),
           (False, u"Неверно"),)


class CheckboxForm(forms.ModelForm):
    question = None
    answer_text = None
    answer = None

    def __init__(self, question, answer_text, answer, *args, **kwargs):
        super(CheckboxForm, self).__init__(*args, **kwargs)
        self.question = question
        self.answer_text = answer_text
        self.answer = answer

    class Meta:
        model = UserAnswer
        fields = ('is_correct',)
        widgets = {
             'is_correct': forms.RadioSelect(choices=CHOICES),
        }
