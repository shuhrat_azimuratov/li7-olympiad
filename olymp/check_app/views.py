# coding=utf-8
from datetime import timedelta
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models import Count, Max
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
# Create your views here.
from django.utils import timezone

from check_app.data_structures.form_keeper import FormKeeper
from check_app.data_structures.get_form import get_form
from check_app.data_structures.struct_res import Student
from check_app.forms import CheckboxForm
from check_app.models import Result, ConfirmationPart
from olymp import settings
from test_app.models import Submission, Subject, UserAnswer
from userman.models import Contestant, Rayon, School
import logging



class Submissions:
    def __init__(self, subm_id, user, css, teacher, is_linked):
        self.id = subm_id
        self.user = user
        self.css = css
        self.teacher = teacher
        self.is_linked = is_linked


@login_required()
def index(request):
    if not request.user.is_staff:
        return HttpResponseRedirect(reverse('test-app:process'))
    countall = Submission.objects.all().count()
    submissions = Submission.objects.filter(is_checked=False).order_by('date_started')
    res = []
    checked_count = Submission.objects.filter(checked_by=request.user).count()
    context = {'all': countall, 'checked': countall - submissions.count(), 'count': checked_count}
    submissions = submissions[:25]
    dt = timedelta(hours=0, minutes=5, seconds=0)
    is_linked = True

    for submission in submissions:
        user = submission.contestant.user.last_name + " " + submission.contestant.user.first_name + \
               " " + submission.contestant.fathers_name
        css = "fa fa-question-circle"
        teacher = ''
        if (submission.start_checking is not None and timezone.now() < submission.start_checking + dt) and (
                    request.user != submission.checked_by):
            teacher = submission.checked_by
            css = 'glyphicon glyphicon-lock'
        if submission.is_checked:
            css = 'fa fa-check-circle'
        res.append(Submissions(subm_id=submission.id, user=user, css=css, teacher=teacher, is_linked=is_linked))
    context['submissions'] = res
    return render(request, 'check_app/index.html', context=context)


@login_required()
def user_check(request, id=-1):
    if not request.user.is_staff:
        return HttpResponseRedirect(reverse('test-app:process'))
    global submission
    id = int(id)

    try:
        submission = Submission.objects.get(pk=id)
        dt = timedelta(hours=0, minutes=5, seconds=0)
        if submission.start_checking is None or timezone.now() >= submission.start_checking + dt:
            submission.start_checking = timezone.now()
            submission.checked_by = request.user
            submission.save()
        elif submission.checked_by != request.user:
            return HttpResponseRedirect(reverse('check:home'))
    except Submission.DoesNotExist:
        print 'No such submission'

    try:
        subject = Subject.objects.get(name__icontains=u'Естествознание')
    except Subject.DoesNotExist:
        print 'No such subject'

    answers = UserAnswer.objects.filter(submission=submission, question__subject=subject)
    forms = []
    for answer in answers:
        atext = answer.answer_text
        qtext = answer.question.text
        ans = answer.question.answer_set.all()
        if ans.count() > 0:
            ans = ans[0]
        form = CheckboxForm(qtext, atext, ans, instance=answer, prefix=str(answer.id))
        forms.append(form)

    if request.POST:
        forms = []
        valid = True
        for answer in answers:
            atext = answer.answer_text
            qtext = answer.question.text
            ans = answer.question.answer_set.all()
            if ans.count > 0:
                ans = ans[0]
            form = CheckboxForm(qtext, atext, ans, request.POST, instance=answer, prefix=str(answer.id))
            forms.append(form)
            if form.is_valid():
                obj = form.save()
            else:
                valid = False
        if valid:
            submission.is_checked = True
            submission.checked_by = request.user
            submission.save()

            return HttpResponseRedirect(reverse('check:home'))
    return render(request, 'check_app/user_check_page.html', {'forms': forms})


def list(request):
    countall = Submission.objects.all().count()
    context = {'all': countall, 'checked': countall - Submission.objects.filter(is_checked=False).count()}
    lst = []

    obj = Submission.objects.all().values('checked_by').exclude(checked_by=None).annotate(
        total=Count('checked_by')).order_by('-checked_by')
    for elem in obj:
        tmp = {'count': elem['total'], 'user': User.objects.get(pk=elem['checked_by']).username}
        lst.append(tmp)
    print tmp
    context['list'] = lst
    return render(request, 'check_app/list.html', context)


def results(request):
    context = {}
    submissions = Submission.objects.all()
    res = []
    res2 = []
    k = 0
    for submission in submissions:
        sum = 0
        k += 1
        print k
        uanswers = UserAnswer.objects.filter(submission=submission)
        for answer in uanswers:
            question = answer.question
            if question.type == 'R':
                ans = answer.answer.all()
                if ans.count() > 0:
                    if ans[0].is_correct:
                        sum += 1
            elif question.type == 'I':
                answer_text = answer.answer_text
                correct_answer_text = question.answer_set.all()[0].text
                if answer_text is not None and correct_answer_text is not None:
                    answer_text = answer_text.lower()
                    answer_text = answer_text.replace(',', '.').strip()
                    correct_answer_text = correct_answer_text.lower()
                    correct_answer_text = correct_answer_text.replace(',', '.').strip()
                    if correct_answer_text == answer_text:
                        sum += 1
            elif question.type == 'A':
                if answer.is_correct:
                    sum += 1
        if len(uanswers) > 30:
            sum /= 3
            sum = int(sum)
        tmp = Result()
        tmp.contestant = submission.contestant
        tmp.score = sum
        res2.append(tmp)
        name = submission.contestant.user.last_name + " " + submission.contestant.user.first_name + \
               " " + submission.contestant.fathers_name
        res.append(Student(name, sum))
    Result.objects.bulk_create(res2)
    res.sort(key=lambda x: x.score, reverse=True)
    pl = 1
    for i in res:
        i.place = pl
        pl += 1
    context['students'] = res

    return render(request, 'check_app/raiting.html', context)


def stats(request):
    context = {}
    submissions = Submission.objects.all()
    res = []
    subjects = Subject.objects.all()
    stat_res = {}
    time_res = {}
    question_count = {}

    for i in subjects:
        stat_res[i] = 0
        time_res[i] = timedelta(hours=0, minutes=0, seconds=0)
        tmp = i.question_set.all().aggregate(Max('question_number'))
        question_count[i] = tmp['question_number__max']
    for submission in submissions:
        sum = 0
        uanswers = UserAnswer.objects.filter(submission=submission)
        for answer in uanswers:

            question = answer.question

            if question.type == 'R':
                ans = answer.answer.all()
                if ans.count() > 0:
                    if ans[0].is_correct:
                        sum += 1
            elif question.type == 'I':
                answer_text = answer.answer_text
                correct_answer_text = question.answer_set.all()[0].text
                if answer_text is not None and correct_answer_text is not None:
                    answer_text = answer_text.lower()
                    answer_text = answer_text.replace(',', '.').strip()
                    correct_answer_text = correct_answer_text.lower()
                    correct_answer_text = correct_answer_text.replace(',', '.').strip()
                    if correct_answer_text == answer_text:
                        sum += 1
            elif question.type == 'A':
                if answer.is_correct:
                    sum += 1
        name = submission.contestant.user.last_name + " " + submission.contestant.user.first_name + \
               " " + submission.contestant.fathers_name
        res.append(Student(submission, sum))
    res.sort(key=lambda x: x.score, reverse=True)
    pl = 1
    res = res[:162]
    for i in res:
        i.place = pl
        pl += 1

    for subm in res:
        assert isinstance(subm.name, Submission)
        uanswers = UserAnswer.objects.filter(submission=subm.name)
        for answer in uanswers:
            question = answer.question
            try:
                time_res[question.subject] += answer.end_time - answer.start_time
            except TypeError:
                pass
            if question.type == 'R':
                ans = answer.answer.all()
                if ans.count() > 0:
                    if ans[0].is_correct:
                        stat_res[question.subject] += 1

            elif question.type == 'I':
                answer_text = answer.answer_text
                correct_answer_text = question.answer_set.all()[0].text
                if answer_text is not None and correct_answer_text is not None:
                    answer_text = answer_text.lower()
                    answer_text = answer_text.replace(',', '.').strip()
                    correct_answer_text = correct_answer_text.lower()
                    correct_answer_text = correct_answer_text.replace(',', '.').strip()
                    if correct_answer_text == answer_text:
                        stat_res[question.subject] += 1
            elif question.type == 'A':
                if answer.is_correct:
                    stat_res[question.subject] += 1

    for k in stat_res:
        stat_res[k] /= len(res)
    print stat_res
    for k in time_res:
        tmp = time_res[k]
        assert isinstance(tmp, timedelta)
        print tmp.total_seconds()
        time_res[k] = tmp.total_seconds() / (question_count[k] * len(res))
    print time_res

    return render(request, 'check_app/raiting.html', context)


def gen_pdf(request, uid):
    import os
    pdf_path = ""
    log = logging.getLogger(__name__)
    try:
        result = Result.objects.get(pk=uid)
        uid = result.contestant.id
        rid = result.id
        pth = os.path.join(settings.BASE_DIR, '..' '/media/certs')
        print pth
        pdf_path = "%s/%s.pdf" % (pth, uid)
        cmd = 'xvfb-run -- /usr/bin/wkhtmltopdf -O Landscape http://127.0.0.1:8000/check/pdf/%s %s' % (
            rid, pdf_path)
        log.debug(pth)
        log.debug(cmd)
        if not os.path.exists(pdf_path):
            print "here"
            log.debug(os.system(cmd))
    except Result.DoesNotExist:
        print ""
    print cmd
    with open(pdf_path, 'rb') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'filename=cert_of_contestant.pdf'
        return response
    pdf.closed
    return HttpResponse("Error")


def get_pdf(request, uid):
    context = {}
    try:
        result = Result.objects.get(pk=uid)
        contestant = result.contestant
        context['name'] = contestant.user.last_name + " " + contestant.user.first_name + " " + contestant.fathers_name
        context['name'] = context['name'].title()
        context['score'] = result.score
        context['school'] = contestant.school.school
        context['gender'] = False
        if contestant.gender == 'M':
            context['gender'] = True
    except Result.DoesNotExist:
        print("Doesnt exist such result %s" % uid)
    return render(request, 'check_app/main.html', context)


def gen_pdf_onsite(request, uid):
    import os
    pdf_path = ""
    try:
        result = Result.objects.get(pk=uid)
        uid = result.contestant.id
        rid = result.id
        pth = os.path.join(settings.BASE_DIR, '..' '/media/certs2')
        print pth
        pdf_path = "%s/%s.pdf" % (pth, uid)
        cmd = 'xvfb-run -- /usr/bin/wkhtmltopdf -O Landscape http://127.0.0.1:8000/check/pdf2/%s %s' % (
            rid, pdf_path)
        # cmd = '/usr/local/bin/wkhtmltopdf -O Landscape http://127.0.0.1:8000/check/pdf2/%s %s' % (
        #     rid, pdf_path)
        print pth
        if not os.path.exists(pdf_path):
            print "here"
            print os.system(cmd)
    except Result.DoesNotExist:
        print ""
    print cmd
    with open(pdf_path, 'rb') as pdf:
        response = HttpResponse(pdf.read(), content_type='application/pdf')
        response['Content-Disposition'] = 'filename=cert_of_contestant.pdf'
        return response
    pdf.closed
    return HttpResponse("Error")


def get_pdf_nosite(request, uid):
    context = {}
    try:
        result = Result.objects.get(pk=uid)
        contestant = result.contestant
        context['name'] = contestant.user.last_name + " " + contestant.user.first_name + " " + contestant.fathers_name
        context['name'] = context['name'].title()
        context['school'] = contestant.school.school
        context['gender'] = False
        if contestant.gender == 'M':
            context['gender'] = True
    except Result.DoesNotExist:
        print ("Doesnt exist such result %s" % uid)
    return render(request, 'check_app/onsite.html', context)


def get_list_of_participants(request):
    context = {}
    results2 = Result.objects.filter(score__gte=15).order_by('-score')
    res2 = []

    for res in results2:

        tmp = {
            'name': res.contestant.user.last_name + " " + res.contestant.user.first_name + " " + res.contestant.fathers_name,
            'school': res.contestant.school.school, 'district': res.contestant.school.rayon.rayon_name,
            'phone': res.contestant.phone_number,
            'gender': res.contestant.gender,
            'class': res.contestant.grade,
            'score': res.score,
            'email': res.contestant.user.email
        }
        try:
            tmp2 = ConfirmationPart.objects.get(contestant=res.contestant)
            tmp['will'] = u'Да'
        except ConfirmationPart.DoesNotExist:
            tmp['will'] = u'Нет'
        res2.append(tmp)
    context = {'students': res2}
    return render(request, 'check_app/participants.html', context)

@login_required()
def questions_list(request, subject_id=1):
    if request.user.is_staff:
        subjects = request.user.subject_set.all()
        if request.user.is_superuser:
            subjects = Subject.objects.all()
        subject = Subject.objects.get(pk=int(subject_id))
        print subject
        assert isinstance(subject, Subject)
        print subject.id
        # for
        questions = subject.question_set.all()
        forms = []
        for question in questions:
            forms.append(FormKeeper(get_form(question), question.question_number, question.text))
        forms.sort()
        context = {'title': subject.name + ':' + str(len(questions)),
                   'forms': forms,
                   'subjects': subjects}
        return render(request, 'check_app/questions.html', context)
    return HttpResponseRedirect(reverse('test-app:home'))


@login_required()
def statistics_participants(request):
    number_participants = len(Submission.objects.all())
    context = {}
    context['sum'] = number_participants
    male = len(Submission.objects.all().filter(contestant__gender='M'))
    female = len(Submission.objects.all().filter(contestant__gender='F'))
    context['male'] = male
    context['female'] = female
    schl = Contestant.objects.values('school').annotate(num_values=Count('school'))
    print schl
    for school in schl:
        x = None
    print number_participants, male, female
    return render(request, 'check_app/statistcs_pariticipants.html', context)