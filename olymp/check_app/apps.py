from __future__ import unicode_literals

from django.apps import AppConfig


class CheckAppConfig(AppConfig):
    name = 'check_app'
