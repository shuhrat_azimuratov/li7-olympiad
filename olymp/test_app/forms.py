from django import forms
import random
from test_app.models import Choice, Answer


class RadioForm(forms.Form):
    type = 'R'
    choices = forms.ModelChoiceField(queryset=Choice.objects.all().order_by('?'),
                                     required=False, widget=forms.RadioSelect(attrs={'class': 'radio'}),
                                     empty_label=None)

    def __init__(self, question, initial=None, *args, **kwargs):
        super(RadioForm, self).__init__(*args, **kwargs)
        self.fields['choices'] = forms.ModelChoiceField(queryset=Answer.objects.filter(question=question).order_by('?'),
                                                        required=False,
                                                        widget=forms.RadioSelect(attrs={'class': 'radio'}),
                                                        empty_label=None,
                                                        initial=initial)


class CheckboxForm(forms.Form):
    type = 'C'
    choices = forms.ModelMultipleChoiceField(queryset=Choice.objects.all().order_by('?'), required=False,
                                             widget=forms.CheckboxSelectMultiple())

    def __init__(self, question, initial=None, *args, **kwargs):
        super(CheckboxForm, self).__init__(*args, **kwargs)
        self.fields['choices'] = forms.ModelMultipleChoiceField(
            queryset=Answer.objects.filter(question=question).order_by('?'), required=False,
            widget=forms.CheckboxSelectMultiple(), initial=initial)


class InputForm(forms.Form):
    type = 'I'
    answer = forms.CharField(max_length=150, initial="HI", widget=forms.TextInput(attrs={'class': 'form-control',
                                                                                         'autofocus': 'autofocus'}))

    def __init__(self, initial=None, *args, **kwargs):
        super(InputForm, self).__init__(*args, **kwargs)
        self.fields['answer'] = forms.CharField(max_length=150, initial=initial,
                                                widget=forms.TextInput(attrs={'class': 'form-control',
                                                                              'autofocus': 'autofocus',
                                                                              'autocomplete': "off"}))


class TextAreaForm(forms.Form):
    type = 'A'
    answer = forms.CharField(max_length=150, initial='Hi', widget=forms.Textarea(attrs={'class': 'form-control',
                                                                                        'rows': '8',
                                                                                        'style': 'min-width: 100%',
                                                                                        'autofocus': 'autofocus'}))

    def __init__(self, initial=None, *args, **kwargs):
        super(TextAreaForm, self).__init__(*args, **kwargs)
        self.fields['answer'] = forms.CharField(max_length=150, initial=initial,
                                                widget=forms.Textarea(attrs={'class': 'form-control',
                                                                             'rows': '8',
                                                                             'style': 'min-width: 100%',
                                                                             'autofocus': 'autofocus',
                                                                             'autocomplete': "off"}))
