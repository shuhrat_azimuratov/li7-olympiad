from django.conf.urls import patterns, include, url

urlpatterns = patterns('test_app.views',
                       # Examples:
                       # url(r'^$', 'tosterproject.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r"^$", 'home', name='home'),
                       url(r"^process/(?P<question_number>[0-9]+)/", 'process', name='process'),
                       url(r"^process/$", 'process', name='process'),
                       url(r"^update/", 'update_db', name='update_db'),
                       url(r'^finished/', 'finished', name='finished'),
                       url(r'^finally-finished/', 'finally_finished', name='finally-finished'),
                       url(r'^timeout/', 'timeout', name='timeout'),
                       url(r'^html/', 'test_html', name='test_html'),
                       url(r'^res/', 'results', name='res'),
                       url(r'^final/','final_results', name='final'),
                       url(r'^confirm/', 'confirm_part', name='confirm'),
                       url(r'^confirm_1st/', 'confirm_1st', name='confirm_1st'),
                       url(r'^not_started/', 'olymp_not_started', name='not_started')


                       # url(r'^login/$', LoginView.as_view(), name='login'),
                       # url(r'^logout/$', 'log_out', name='logout'),
                       # url(r'^class/(?P<class_id>\d+)/$', 'class_journal', name='class_journal'),
                       # url(r'^test/$', 'raiting2', name='raiting2'),
                       # url(r'^class/(?P<class_id>\d+)/master$', 'class_journal', name='my_class'),
                       # url(r'^student/(?P<student_id>\d+)/$', 'student_profile', name='student'),
                       # url(r'^raiting/$', 'raiting', name='raiting'),
                       # url(r'^inc/$', 'increase', name='inc')
                       # url(r'^event/(?P<event_id>\d+)/edit/$', 'event_edit', name='event_edit')
                       )
