# coding=utf-8
from django.contrib import admin
from django import forms
from redactor.widgets import RedactorEditor
# Register your models here.

from test_app.models import Question, Test, Answer, Submission, UserAnswer, Choice, AnswerTmp, QuestionTmp, Subject, \
    LastUsed, UsedTimes, UserVariant


class QuestionAdminForm(forms.ModelForm):
    class Meta:
        model = Question
        widgets = {
            'text': RedactorEditor(),
        }
        fields = ['subject', 'type', 'text', 'question_number', 'checked_by_comp']


class AnswerInline(admin.StackedInline):
    model = Answer
    extra = 0


class QuestionModel(admin.ModelAdmin):
    form = QuestionAdminForm
    #fields = ('subject', 'type', 'text', 'question_number', 'checked_by_comp')
    list_display = ['type', 'text', 'subject', 'question_number', 'checked_by_comp']
    inlines = [AnswerInline]
    search_fields = ['text', 'subject__name']
    list_filter = ('subject__name',)


class LastUsedAdmin(admin.ModelAdmin):
    list_display = ('school', 'subject', 'question_number', 'last_used')


class UsedTimesAdmin(admin.ModelAdmin):
    list_display = ('school', 'subject', 'question', 'used_times')


class VariantAdmin(admin.ModelAdmin):
    list_display = ('contestant', 'question', 'number')


class SubmissionAdmin(admin.ModelAdmin):
    list_display = ('get_contestant', 'date_started', 'date_ended', 'test')

    def get_contestant(self, obj):
        return obj.contestant.user.first_name + ' ' + obj.contestant.user.last_name + ' ' + obj.contestant.fathers_name


class AnswerModelForm(forms.ModelForm):
    class Meta:
        model = Answer
        widgets = {
            'text': RedactorEditor(),
        }
        fields = ['text']


class AnswerAdmin(admin.ModelAdmin):
    form = AnswerModelForm
    list_display = ('subject', 'question_number', 'question', 'text', 'is_correct',)

    def subject(self, obj):
        return obj.question.subject

    def question_number(self, obj):
        return obj.question.question_number

    subject.short_description = u'Предмет'
    question_number.short_description = u'Номер вопроса'


admin.site.register(Question, QuestionModel)
admin.site.register(Test)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(UserAnswer)
admin.site.register(AnswerTmp)
admin.site.register(Subject)
admin.site.register(LastUsed, LastUsedAdmin)
admin.site.register(UsedTimes, UsedTimesAdmin)
admin.site.register(UserVariant, VariantAdmin)
