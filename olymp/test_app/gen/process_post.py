from django.utils import timezone
from test_app.forms import RadioForm, CheckboxForm, InputForm, TextAreaForm
from test_app.models import UserVariant, UserAnswer


def process_post(request, contestant, question_number):
    global uanswer
    try:
        variant = UserVariant.objects.get(contestant=contestant, number=question_number)
    except UserVariant.DoesNotExist:
        print('No such question')
    question = variant.question
    forms = {'R': RadioForm, 'C': CheckboxForm, 'I': InputForm, 'A': TextAreaForm}
    form = None
    mc = False
    try:
        uanswer = UserAnswer.objects.get(submission__contestant=contestant, question=question)
    except UserAnswer.DoesNotExist:
        pass
    for i in forms:
        if question.type == i:
            if i in ['R', 'C']:
                form = forms[i](question, None, request.POST)
                mc = True
            else:
                form = forms[i](None, request.POST)
    if form.is_valid():
        print mc
        if mc:
            uanswer.answer.clear()
            if form.type == 'R':
                if form.cleaned_data['choices']:
                    uanswer.answer.add(form.cleaned_data["choices"])
                else:
                    return
            else:
                for choice in form.cleaned_data["choices"]:
                    uanswer.answer.add(choice)
        else:
            uanswer.answer_text = form.cleaned_data["answer"]
        variant.is_answered = True
        variant.save()
        uanswer.end_time = timezone.now()
        uanswer.save()
