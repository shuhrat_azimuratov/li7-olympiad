from django.db import IntegrityError

from test_app.models import LastUsed, UsedTimes, UserVariant, Submission, Test
from userman.models import Contestant
from random import shuffle
from django.utils import timezone
import logging


def generator(contestant):
    # contestant = Contestant.objects.get(user=user)

    school = contestant.school
    res = []
    quest_no = LastUsed.objects.filter(school=school)

    try:
        submission = Submission()
        submission.contestant = contestant
        submission.date_started = timezone.now()
        submission.test = Test.objects.first()
        submission.save()
        print submission.id, submission.contestant

    except IntegrityError:
        print 'Oopes'

    for qn in quest_no:
        ut = UsedTimes.objects.filter(school=school, question__subject=qn.subject,
                                      question__question_number=qn.question_number,
                                      used_times__lte=qn.last_used).order_by('?')

        if len(ut) == 1:
            qn.last_used += 1
            qn.save()
        if len(ut) != 0 and ut is not None:
            ut = ut[0]
            ut.used_times += 1
            variant = UserVariant()
            variant.question = ut.question
            variant.contestant = contestant
            res.append(variant)
            ut.save()
    shuffle(res)

    for i in range(0, len(res)):
        res[i].number = i + 1

    UserVariant.objects.bulk_create(res)

