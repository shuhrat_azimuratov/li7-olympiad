class Progress:
    def __init__(self, question_number, style_class):
        self.question_number = question_number
        self.style_class = style_class

    def __str__(self):
        return str(self.question_number) + " " + self.style_class
