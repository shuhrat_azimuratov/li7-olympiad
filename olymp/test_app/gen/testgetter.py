from time import mktime

from django.db import IntegrityError
from django.utils import timezone
from test_app.forms import RadioForm, CheckboxForm, InputForm, TextAreaForm
from test_app.gen.ProgressClass import Progress
from test_app.gen.generator import generator
from test_app.models import UserVariant, UserAnswer, Test
from datetime import datetime, timedelta


def get_millis(contestant):
    test = Test.objects.first()
    submission = contestant.submission_set.all()[0]
    assert isinstance(test, Test)
    dt = timedelta(hours=test.duration.hour, minutes=test.duration.minute, seconds=test.duration.second)
    print dt
    dt = submission.date_started + dt
    return int(mktime(dt.timetuple()) + dt.microsecond / 1000000.0) * 1000


def get_choice(contestant, variant):
    try:
        uanswer = UserAnswer.objects.get(submission__contestant=contestant, question=variant.question)
    except UserAnswer.DoesNotExist:
        return None
    if uanswer.question.type == 'R':
        for choice in uanswer.answer.all():
            return choice
    elif uanswer.question.type == 'C':
        res = []
        for choice in uanswer.answer.all():
            res.append(choice)
        return res
    else:
        return uanswer.answer_text


def get_test_form(contestant, problem_num=-1):
    context = {'finished': False, 'timeout': False}
    variant = UserVariant.objects.filter(number=problem_num, contestant=contestant).first()
    test = Test.objects.first()
    try:
        submission = contestant.submission_set.all()[0]
    except IndexError:
        print len(contestant.submission_set.all()[0])
        #generator(contestant)
        #submission = contestant.submission_set.all()[0]
    assert isinstance(test, Test)
    dt = timedelta(hours=test.duration.hour, minutes=test.duration.minute, seconds=test.duration.second)
    dt = submission.date_started + dt  # + test.duration
    context['time'] = int(mktime(dt.timetuple()) + dt.microsecond / 1000000.0) * 1000
    if problem_num > int(UserVariant.objects.filter(contestant=contestant).count()):
        context['finished'] = True
        return context

    if dt < timezone.now():
        contestant.finished = True
        contestant.save()
        context['timeout'] = True
        return context
    if variant is None:
        return context
    context['statement'] = variant.question.text
    forms = {'R': RadioForm, 'C': CheckboxForm, 'I': InputForm, 'A': TextAreaForm}

    for i in forms:
        if i == variant.question.type:
            if i in ['R', 'C']:
                context['form'] = forms[i](variant.question, get_choice(contestant, variant))
            else:
                context['form'] = forms[i](get_choice(contestant, variant))

    res = []
    all_questions = UserVariant.objects.filter(contestant=contestant).order_by('number')
    print len(all_questions)
    for question in all_questions:
        style_class = 'default'
        if question.is_answered:
            style_class = 'warning'

        if question.number == problem_num:
            style_class = 'primary'

        tmp = Progress(question.number, style_class)
        res.append(tmp)
    try:
        uanswer = UserAnswer()
        uanswer.submission = contestant.submission_set.all()[0]
        uanswer.question = variant.question
        uanswer.start_time = timezone.now()
        uanswer.save()
    except IntegrityError:
        pass

    context['question_number'] = problem_num
    context['progress'] = res

    return context
