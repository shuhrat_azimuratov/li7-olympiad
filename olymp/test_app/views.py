from datetime import timedelta, datetime
from time import mktime
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
# Create your views here.
from django.utils import timezone

from check_app.models import Result, ConfirmationPart
from olymp.settings import HAS_STARTED
from test_app.forms import RadioForm, CheckboxForm, InputForm, TextAreaForm
from test_app.gen.generator import generator
from test_app.gen.process_post import process_post
from test_app.gen.testgetter import get_test_form, get_millis
from test_app.models import AnswerTmp, Question, UsedTimes, Test, Subject, LastUsed, Submission, UserAnswer
from test_app.sturcts.log_struct import LogStruct
from userman.forms import ConfirmationForm
from userman.models import Contestant, School


def check_if_finished(request):
    test = Test.objects.first()
    assert isinstance(test, Test)
    # print timezone.now().microsecond, test.end_date, test.end_date < datetime.now()
    # return False
    print test.end_date < timezone.now()
    # On the olymp
    return test.end_date < timezone.now()


@login_required()
def confirm_1st(request):
    contestant = Contestant.objects.filter(user=request.user)
    if len(contestant) != 0:
        contestant = contestant.first()
        assert isinstance(contestant, Contestant)

        if not HAS_STARTED:
            return render(request, 'test_app/not_started.html')
        if contestant.started:
            return HttpResponseRedirect(reverse('test-app:home'))
        else:
            return render(request, 'test_app/base.html')


@login_required()
def home(request):
    print "Here"
    if request.user.is_staff:
        return HttpResponseRedirect(reverse('check:questions_list'))
    if not request.user.is_active:
        return render(request, 'test_app/activation_required.html')
    if not HAS_STARTED:
        return render(request, 'test_app/not_started.html')
    # if not HAS_STARTED:
    #     return HttpResponseRedirect(reverse('test-app:not_started'))
    contestant = Contestant.objects.filter(user=request.user)
    if len(contestant) != 0:
        contestant = contestant.first()
        assert isinstance(contestant, Contestant)

        if not contestant.started:
            return HttpResponseRedirect(reverse('test-app:confirm_1st'))
        if contestant.finished:
            return HttpResponseRedirect(reverse('test-app:process'))
        if contestant.started:
            return HttpResponseRedirect(reverse('test-app:process'))
        if check_if_finished(request):
            return render(request, 'test_app/olympf.html')
    # return HttpResponseRedirect(reverse('test-app:process'))
    return render(request, 'test_app/base.html')


@login_required()
def process(request, question_number=-1):
    print "Here 111"
    if not request.user.is_active:
        return render(request, 'test_app/activation_required.html')
    if request.user.is_staff:
        return HttpResponseRedirect(reverse('check:home'))
    if not HAS_STARTED:
        return render(request, 'test_app/not_started.html')
    try:
        contestant = Contestant.objects.get(user=request.user)
        print "Found"
    except Contestant.DoesNotExist:
        print "Here"
        return render(request, 'test_app/base.html')
    if check_if_finished(request) or contestant.finished:
        print "Heresadasdas"
        return render(request, 'test_app/olympf.html')
    question_number = int(question_number)

    if contestant.finished:
        return HttpResponseRedirect(reverse('test-app:finally-finished'))
    context = {}
    if request.POST:
        process_post(request, contestant, question_number)
        question_number += 1

    if not contestant.started:
        contestant.started = True
        contestant.save()
        generator(contestant)

    if question_number == -1:
        question_number = 1

    context = get_test_form(contestant, question_number)
    # print context['time']
    if context['finished']:
        return HttpResponseRedirect(reverse('test-app:finished'))
    if context['timeout']:
        return HttpResponseRedirect(reverse('test-app:timeout'))

    return render(request, 'test_app/test.html', context)


@login_required()
def finished(request):
    if not HAS_STARTED:
        return render(request, 'test_app/not_started.html')
    contestant = Contestant.objects.get(user=request.user)
    return render(request, 'test_app/finished.html', {'time': get_millis(contestant)})


@login_required()
def finally_finished(request):
    if not HAS_STARTED:
        return render(request, 'test_app/not_started.html')
    try:
        contestant = Contestant.objects.get(user=request.user)
        contestant.finished = True
        contestant.save()
    except Contestant.DoesNotExist:
        pass
    return HttpResponseRedirect(reverse('logoutt'))


@login_required()
def update_db(request):
    sum1 = 0
    sum2 = 0
    useds = []
    lus = []
    if request.user.is_superuser:
        # used_times init
        schools = School.objects.all()
        subjects = Subject.objects.all()
        questions = Question.objects.all()
        test = Test.objects.first()
        questions2 = {}
        for subject in subjects:
            questions2[subject.name] = Question.objects.filter(subject=subject).values('question_number').distinct()
        for school in schools:
            for question in questions:
                sum1 += 1
                used = UsedTimes()
                used.school = school
                used.subject = question.subject
                used.question = question
                used.used_times = 0
                used.test = test
                useds.append(used)
        for school in schools:
            for subject in subjects:
                for question in questions2[subject.name]:
                    sum2 += 1
                    lu = LastUsed()
                    lu.subject = subject
                    lu.school = school
                    lu.question_number = question['question_number']
                    lu.test = test
                    lu.last_used = 0
        #             lus.append(lu)
        # LastUsed.objects.bulk_create(lus)
        # UsedTimes.objects.bulk_create(useds)
    return HttpResponse('UT = ' + str(sum1) + " LU " + str(sum2))


@login_required()
def timeout(request):
    return render(request, 'test_app/timeout.html')


@login_required()
def test_html(request):
    return render(request, 'test_app/activation_required.html')


@login_required()
def results(request):
    global contestant, submission
    try:
        contestant = Contestant.objects.get(user=request.user)
    except Contestant.DoesNotExist:
        print "No such contestant"
    try:
        submission = Submission.objects.get(contestant=contestant)
    except:
        print "No submission defined with this contestant"
    res = []
    uanswers = UserAnswer.objects.filter(submission=submission)
    sum = 0
    print len(uanswers)
    for answer in uanswers:
        question = answer.question
        txt = ""
        cans = ""
        if question.type == 'R':
            ans = answer.answer.all()
            if ans.count() > 0:
                txt = ans[0].text
                cans = question.answer_set.all().get(is_correct=True).text
                if ans[0].is_correct:
                    sum += 1
        elif question.type == 'I':
            answer_text = answer.answer_text
            correct_answer_text = question.answer_set.all()[0].text
            cans = correct_answer_text
            txt = answer_text
            if answer_text is not None and correct_answer_text is not None:
                answer_text = answer_text.lower()
                answer_text = answer_text.replace(',', '.').strip()
                correct_answer_text = correct_answer_text.lower()
                correct_answer_text = correct_answer_text.replace(',', '.').strip()
                if correct_answer_text == answer_text:
                    sum += 1
        elif question.type == 'A':
            txt = answer.answer_text
            try:
                cans = question.answer_set.all().get(is_correct=True).text
            except:
                print "Oop"
            if answer.is_correct:
                sum += 1
        res.append(LogStruct(question=question.text, uans=txt, cans=cans))

    context = {'score': sum, 'res': res, 'passed': sum >= 15, 'uid': contestant.result_set.all()[0].id}
    return render(request, 'test_app/review.html', context)


def final_results(request):
    context = {}
    res = Result.objects.all()
    res = res[:114]
    ind = 1
    res2 = []
    for i in res:
        assert isinstance(i, Result)
        tmp = {
            'user': i.contestant.user.last_name + " " + i.contestant.user.first_name + " " + i.contestant.fathers_name,
            'school': i.contestant.school.school,
            'district': i.contestant.school.rayon,
            'score': i.score,
            'place': ind
        }
        if i.score >= 19:
            res2.append(tmp)
            ind += 1
    context['res'] = res2

    return render(request, 'test_app/results.html', context)


@login_required()
def confirm_part(request):
    global cont
    passed = False
    try:
        result = Result.objects.get(contestant__user=request.user)
        passed = result.score >= 34
    except Result.DoesNotExist:
        print "OOps"
    if not passed:
        return HttpResponseRedirect(reverse('test-app:final'))
    form = ConfirmationForm()
    context = {'form': form}
    if request.POST:
        form = ConfirmationForm(request.POST)
        if form.is_valid():
            tmp = ConfirmationPart()
            try:
                cont = Contestant.objects.get(user=request.user)
            except:
                print "oopss"
            tmp.contestant = cont
            tmp.phone_number = form.cleaned_data['phone']
            tmp.who = form.cleaned_data['who']
            tmp.will_come = form.cleaned_data['will_come']

            if form.cleaned_data['will_come'] and passed:
                print form.cleaned_data['will_come']
                tmp.save()
                return render(request, 'test_app/confirm.html')

    return render(request, 'userman/confirmpart.html', context)


@login_required()
def olymp_not_started(request):
    return render(request, 'test_app/not_started.html')
