# coding=utf-8
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
# Create your models here.
from django.utils.safestring import mark_safe
from userman.models import Contestant, School
from redactor.fields import RedactorField


# Subject name we want to check
# help_text - help text wich will be displayed before starting writing test
class Subject(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'Название предмета')
    help_text = RedactorField(
        verbose_name=u'Инструкция к предмету',
        redactor_options={'lang': 'en', 'focus': 'true'},
        upload_to='questions/',
        allow_file_upload=True,
        allow_image_upload=True
    )
    teacher = models.ForeignKey(User, verbose_name=u'Учитель')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Предмет'
        verbose_name_plural = u'Предметы'


# Test - class that combines several subjects and start time, end time, and duration of submission.
class Test(models.Model):
    subject = models.ManyToManyField(Subject, verbose_name=u'Предмет')
    name = models.CharField(max_length=200, verbose_name=u'Отображаемое имя')
    start_date = models.DateTimeField(verbose_name=u'Время начала')
    end_date = models.DateTimeField(verbose_name=u'Время конца')
    duration = models.TimeField(verbose_name='Время написания теста')
    number_of_questions = models.IntegerField(default=1, verbose_name=u'Количество вопросов')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"Тест"
        verbose_name_plural = u'Тесты'


# Question describes a question in a subject. Text - question text. Help_text - if provided will be displayed
class Question(models.Model):
    subject = models.ForeignKey(Subject)
    text = RedactorField(
        verbose_name=u'Текст вопроса',
        redactor_options={'lang': 'en', 'focus': 'true'},
        allow_file_upload=True,
        allow_image_upload=True
    )
    help_text = RedactorField(
        verbose_name=u'Подсказка',
        redactor_options={'lang': 'en', 'focus': 'true'},
        allow_file_upload=True,
        allow_image_upload=True,
        blank=True,
        null=True
    )
    checked_by_comp = models.BooleanField(default=True, verbose_name=u'Проверяет компьютер?')
    RADIO = "R"
    CHECKBOX = "C"
    TXTAREA = "A"
    TXTINPUT = "I"
    TYPE_CHOICES = ((RADIO, u"Один из списка"),
                    (CHECKBOX, u"Несколько из списка"),
                    (TXTAREA, u"Текст(абзац)"),
                    (TXTINPUT, u"Текст(строка)"))
    type = models.CharField(max_length=1, choices=TYPE_CHOICES)
    # todo Добавить выпадающий список в отображении
    question_number = models.IntegerField(verbose_name=u'Номер вопроса')

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'


# Last used model describes how many times from one school defined question were used
class LastUsed(models.Model):
    school = models.ForeignKey(School, verbose_name=u'Школа')
    test = models.ForeignKey(Test, verbose_name=u'Тест')
    subject = models.ForeignKey(Subject, verbose_name=u'Предмет')
    question_number = models.IntegerField(verbose_name=u'Номер вопроса')
    last_used = models.IntegerField(default=-1, verbose_name=u'Кол-во раз')

    def __unicode__(self):
        return str(self.question_number) + str(self.last_used)


# How many times from one school the test were written
class UsedTimes(models.Model):
    test = models.ForeignKey(Test, blank=True, null=True, verbose_name='Тест')
    school = models.ForeignKey(School, verbose_name=u'Школа')
    subject = models.ForeignKey(Subject, verbose_name=u'Предмет')
    question = models.ForeignKey(Question, verbose_name=u'Вопрос')
    used_times = models.IntegerField(default=-1, verbose_name=u'Использованное кол-во')

    def __unicode__(self):
        return str(self.used_times)


# Variants of Answers for the question
class Answer(models.Model):
    question = models.ForeignKey(Question)
    text = RedactorField(
        verbose_name=u'Текст ответа',
        redactor_options={'lang': 'en', 'focus': 'true'},
        upload_to='answers/',
        allow_file_upload=True,
        allow_image_upload=True
    )
    is_correct = models.BooleanField(default=False, verbose_name=u'Правильный?')

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name_plural = u'Варианты ответов'
        verbose_name = u'вариант ответа'


# Submission contains start time and end time
class Submission(models.Model):
    contestant = models.ForeignKey(Contestant, verbose_name=u'Участник')
    date_started = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата начала')
    date_ended = models.DateTimeField(verbose_name=u'Дата окончания', null=True, blank=True)
    test = models.ForeignKey(Test, null=True, blank=True)
    is_checked = models.BooleanField(default=False, verbose_name=u'Работа проверена?')
    start_checking = models.DateTimeField(verbose_name=u'Начало проверки', null=True, blank=True)
    checked_by = models.ForeignKey(User, verbose_name=u'Проверена?', null=True, blank=True)

    class Meta:
        verbose_name = u'Посылка'
        verbose_name_plural = u'Посылки'
        unique_together = ('contestant', 'test')


# User Answer for the question. If type of question is CHECKBOX OR TXTAREA then answer_text is used.
class UserAnswer(models.Model):
    submission = models.ForeignKey(Submission, verbose_name=u'Посылка')
    question = models.ForeignKey(Question, verbose_name=u'Вопрос', blank=True, null=True)
    answer = models.ManyToManyField(Answer, verbose_name=u'Ответ пользователя', blank=True, null=True)
    answer_text = models.CharField(max_length=150, verbose_name=u'текст ответа', blank=True, null=True)
    start_time = models.DateTimeField(verbose_name=u'Время начала ответа на вопрос', null=True, blank=True)
    end_time = models.DateTimeField(verbose_name=u'Время окончания ответа', null=True, blank=True)
    is_correct = models.NullBooleanField(verbose_name=u'Верно?')
    checked = models.BooleanField(verbose_name=u'Проверенно', default=False)

    class Meta:
        verbose_name = u'Ответ участника'
        verbose_name_plural = u'Ответы участников'
        unique_together = ('submission', 'question')


class UserVariant(models.Model):
    contestant = models.ForeignKey(Contestant, verbose_name=u'Участник')
    question = models.ForeignKey(Question, verbose_name=u'Вопрос')
    number = models.IntegerField(verbose_name=u'Порядковый номер')
    is_answered = models.BooleanField(verbose_name=u'Отвечен?', default=False)


class Choice(models.Model):
    name = RedactorField(
        verbose_name=u'Подсказка',
        redactor_options={'lang': 'en', 'focus': 'true'},
        upload_to='questions/',
        allow_file_upload=True,
        allow_image_upload=True
    )

    def __unicode__(self):
        return unicode(mark_safe(self.name))


class QuestionTmp(models.Model):
    name = RedactorField(
        verbose_name=u'Подсказка',
        redactor_options={'lang': 'en', 'focus': 'true'},
        upload_to='questions/',
        allow_file_upload=True,
        allow_image_upload=True
    )
    type = models.CharField(max_length=1, choices=Question.TYPE_CHOICES)
    choice = models.ManyToManyField(Choice)


class AnswerTmp(models.Model):
    question = models.ForeignKey(QuestionTmp, null=True, blank=True)
    choice = models.ManyToManyField(Choice, null=True, blank=True)
    answer = models.CharField(max_length=1000, null=True, blank=True)

    def __unicode__(self):
        return unicode(self.choice.name) or u''
